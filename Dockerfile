FROM node:8.16.0-jessie
RUN apt-get update && apt-get install -y build-essential \
    cmake git rsync software-properties-common gnupg2  \
    apt-transport-https ca-certificates rsync jq curl \
    python-dev vim && rm -rf /var/lib/apt/lists/*
RUN curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py && python get-pip.py
RUN npm install balena-cli -g --production --unsafe-perm